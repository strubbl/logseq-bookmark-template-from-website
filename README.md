# Browser addon logseq-bookmark-template-from-website

## Project Home

https://codeberg.org/strubbl/logseq-bookmark-template-from-website


## Usage of LocalStorage

### Last Tab Info
The addon saves information (URL, title, description, tags) about the last active tab. If that data is not saved, closing and opening the addon's popup via the toolbar icon forgets the changed title, description and tags. If this is not saved, the URL and title are always fetched freshly from the current active tab, but description and tags are empty.

### All entered Tags
For auto-completion of tags when entering a tag, all tags being entered during the lifetime of this addon are saved to the options in the LocalStorage.

### Options
If you change any of the options and save them, they are stored to LocalStorage.
