function updateTemplateText(tab) {
    // TODO get LogseqTemplateText from options
    curText = document.getElementById('logseqTemplateText').value
    curText = curText.replace(/{{url}}/g, tab.theURL)
    curText = curText.replace(/{{title}}/g, tab.title)
    curText = curText.replace(/{{description}}/g, tab.description)
    curText = curText.replace(/{{tags}}/g, tab.tags)
    document.getElementById('logseqTemplateText').value = curText
}

function logCurrentActiveTab(tabs) {
    let tab = tabs[0];
    if (curTab != null && curTab.theURL != tab.url) {
        //console.log("logCurrentActiveTab: curTab.theURL != tab.url", curTab.theURL, tab.url)
        curTab.theURL = tab.url
        curTab.title = tab.title
        curTab.description = ""
        curTab.tags = ""
    } else {
        //console.log("logCurrentActiveTab: curTab.theURL == tab.url")
    }
    document.getElementById('logseqTemplateTitle').value = curTab.title
    document.getElementById('logseqTemplateDescription').value = curTab.description
    document.getElementById('logseqTemplateTags').value = curTab.tags
    updateTemplateText(curTab)
    saveTabInfo(curTab)
}

function saveTabInfo(t) {
    lastTabJSON = JSON.stringify(t)
    localStorage.setItem('lastTab', lastTabJSON)
}

function loadTabInfo() {
    lastTabJSON = localStorage.getItem('lastTab')
    return JSON.parse(lastTabJSON)
}

function loadOptions() {
    optionsJSON = localStorage.getItem('options')
    if (optionsJSON == null) {
        //initialize options
        var options = new Object();
        options.optionLogseqTemplate = defaultLogseqTemplateText
        options.optionLearnedTags = ""
        optionsJSON = JSON.stringify(options)
        localStorage.setItem('options', optionsJSON)
    }
    return JSON.parse(optionsJSON)
}


var curTab = new Object();
document.addEventListener('DOMContentLoaded', function () {
    lastTabFromLocalStorage = loadTabInfo()
    if (lastTabFromLocalStorage != null) {
        curTab = lastTabFromLocalStorage
    }
    options = loadOptions()
    if (document.getElementById('logseqTemplateText').value == "") {
        document.getElementById('logseqTemplateText').value = options.optionLogseqTemplate
    }
    browser.tabs.query({ currentWindow: true, active: true }).then(logCurrentActiveTab, console.error)
    document.getElementById('logseqTemplateTags').value = curTab.tags
}, false)

// Load stored tags
allTagsJSON = localStorage.getItem('allTagsJSON')
//console.log("allTagsJSON from localStorage:", allTagsJSON)
allTags = JSON.parse(allTagsJSON)

function updateDescription(desc) {
    // TODO do not remove the text when the selection is removed. Keep it until a new selection is made
    curTab.description = desc
    updateTemplateText(curTab)
    saveTabInfo(curTab)
}

function handleResponse(message) {
    console.log(`browser_action answered: ${message.response}`)
    updateDescription(message.response)
}

function handleError(error) {
    console.log(`Error: ${error}`)
}

function notifyBackgroundPage(e) {
    const sending = browser.runtime.sendMessage({
        greeting: "Excuse me background script, can you please get me the selection",
    })
    sending.then(handleResponse, handleError)
}

//window.addEventListener("click", notifyBackgroundPage);

notifyBackgroundPage()

//document.getElementById('#logseqTemplateHeading').click(function () {
//    browser.tabs.create({
//        url: 'https://addons.mozilla.org/firefox/addon/'
//    })
//})

logseqTemplateTitle.oninput = () => {
    curTab.title = document.getElementById('logseqTemplateTitle').value
    saveTabInfo(curTab)
}

logseqTemplateDescription.oninput = () => {
    curTab.description = document.getElementById('logseqTemplateDescription').value
    saveTabInfo(curTab)
}

logseqTemplateTags.oninput = () => {
    //console.log("tags input field changed:", logseqTemplateTags.value)
    allTagsJSON = JSON.stringify(allTags)
    // updateTemplateText
    //localStorage.setItem('allTagsJSON', allTagsJSON)
    curTab.tags = document.getElementById('logseqTemplateTags').value
    saveTabInfo(curTab)
}

