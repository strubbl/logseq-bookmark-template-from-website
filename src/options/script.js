var options = new Object();

function saveOptions(e) {
    options.optionLogseqTemplate = document.querySelector("#option-logseqTemplate").value
    options.optionLearnedTags = document.querySelector("#option-learnedTags").value
    localStorage.setItem('options', JSON.stringify(options))
}

function restoreOptions() {
    function setCurrentChoice(result) {
        document.querySelector("#option-logseqTemplate").value = result.optionLogseqTemplate || defaultLogseqTemplateText
        document.querySelector("#option-learnedTags").value = result.optionLearnedTags || ""
    }

    var options = JSON.parse(localStorage.getItem('options'))
    setCurrentChoice(options)
}

document.addEventListener("DOMContentLoaded", restoreOptions)
document.querySelector("form").addEventListener("submit", saveOptions)
