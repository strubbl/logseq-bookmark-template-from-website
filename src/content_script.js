// Put all the javascript code here, that you want to execute after page load.
console.log("content_script.js started")

function handleResponse(message) {
    console.debug(`content_script.js: Message from the background script: ${message.response}`);
}

function handleError(error) {
    console.log(`content_script.js Error: ${error}`);
}

function notifyBackgroundPage(e) {
    //console.debug("content_script.js getSelectedText():", getSelectedText().toString())
    const sending = browser.runtime.sendMessage({
        greeting: getSelectedText().toString(),
    });
    sending.then(handleResponse, handleError);
}

function getSelectedText() {
    if (window.getSelection) {
        return window.getSelection();
    }
    if (window.document.getSelection) {
        return window.document.getSelection();
    }
    if (window.document.selection) {
        return window.document.selection.createRange().text;
    }
    return "";
}

// TODO use the onSelection event here
window.addEventListener("click", notifyBackgroundPage);
