// execute in background

var last_selection = ""

function handleMessage(request, sender, sendResponse) {
    //console.debug("background-script.js: who is the sender:", sender.envType)
    //console.debug(`background-script.js: received message: ${request.greeting}`)
    var responsetext = ""
    if (sender.envType == "content_child") {
        last_selection = request.greeting
        responsetext = "thank you for providing a selection"
    } else if (sender.envType == "addon_child") {
        responsetext = last_selection
    } else {
        console.warn("background-script.js: ohoh, got an unkown sender.envType:", sender.envType)
    }
    sendResponse({ response: responsetext });
}

browser.runtime.onMessage.addListener(handleMessage);
